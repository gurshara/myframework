#include <iostream>
#include <string>
#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TKey.h>

void createHistograms(const std::string& inputFile, const std::string& outputPath, const std::string& directory = "") {
    // Open input ROOT file
    TFile* file = TFile::Open(inputFile.c_str());
    if (!file || file->IsZombie()) {
        std::cerr << "Error: Unable to open input file " << inputFile << std::endl;
        return;
    }

    // Navigate to the specified directory or the top-level directory if not provided
    if (!directory.empty()) file->cd(directory.c_str());

    // Retrieve the list of keys in the current directory
    TList* keys = gDirectory->GetListOfKeys();

    // Loop over each key in the directory
    for (int i = 0; i < keys->GetSize(); ++i) {
        TKey* key = static_cast<TKey*>(keys->At(i));
        TObject* obj = key->ReadObj();

        // Check if the object is a TTree
        TTree* tree = dynamic_cast<TTree*>(obj);
        if (tree) {
            // Create a new ROOT file with the same name as the tree
            std::string outputFileName = outputPath + "/" + tree->GetName() + ".root";
            TFile* outputFile = new TFile(outputFileName.c_str(), "RECREATE");

            // Loop over each branch in the tree
            TObjArray* branches = tree->GetListOfBranches();
            for (int j = 0; j < branches->GetSize(); ++j) {
                TBranch* branch = dynamic_cast<TBranch*>(branches->At(j));
                if (!branch) continue;

                // Get minimum and maximum values of the branch
                double minValue = tree->GetMinimum(branch->GetName());
                double maxValue = tree->GetMaximum(branch->GetName());

                // Create histogram for the branch with dynamic binning
                TH1F* hist = new TH1F(branch->GetName(), branch->GetName(), 100, minValue, maxValue);
                tree->Project(hist->GetName(), branch->GetName());

                // Write histogram to the output file
                outputFile->cd();
                hist->Write();
                delete hist;
            }

            // Close the output file
            outputFile->Close();
            delete outputFile;
        }
    }

    // Close input file
    file->Close();
}


int main(int argc, char* argv[]) {
    if (argc < 3 || argc > 4) {
        std::cerr << "Usage: " << argv[0] << " <input_file.root> <output_path> [directory]" << std::endl;
        return 1;
    }

    std::string inputFile = argv[1];
    std::string outputPath = argv[2];
    std::string directory = (argc == 4) ? argv[3] : "";

    createHistograms(inputFile, outputPath, directory);

    return 0;
}
