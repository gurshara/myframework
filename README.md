hello , everyone this is the path to the file root -l /eos/cms/store/group/dpg_ecal/alca_ecalcalib/ecalelf/ntuples/rchatter/2018_UL_Particle_MC_HighStats/DoubleElectron_FlatPt-1To300/nTupleMC_Merged.root , this is a 38gb file so recommended not to download it just give the file path directly.

conda env create -f environment.yml this command will help you to create my_environment environment which is suitable for this framework.

now i am making code c++ which will take the path of the file and convert it into a histograms and save them into a ntupple in workspace area .
it has the command line feature that will allow you to make histogram within specified Tdirectory of your root , bydefault it will go through all Tdirectory and make a new Tdirectory with the same name and make histogram with the same name .




make run ARGS="input_file.root workspace"
or

make run ARGS="input_file.root workspace specific_directory"


bincrithitogram.cpp will do 

energy    : vector<float>                                          *

energy_ecal : vector<float>                                        *

energy_ecal_mustache :

Ele_Gen_Pt : vector<float>  

pt        : vector<float>      

 filterEvents(inputFileName, outputFileName, branchName, criteria, binSize, directoryName);
 step-1:source bincrithitogram.sh 
 step-2:You got bincrithitogram objective file , so Run this command after bincrithitogram inputfile_address output_address branchname criteria[int] resolution directoryName[if your root file has a directory feature (optional feature)]
 example:
./bincrithitogram /home/gurshara/myframework/rootfile/nTupleMC_Merged.root /home/gurshara/myframework/workspace2/output.root energy 1000 0.5 T /home/gurshara/myframework/workspace2/output.txt nTuplelize


#inputfileaddress #outputfileaddress #branchNamewhosehistograme you want to make with variable slicing #initial binsize guess,  directoryNmae of the ntupples if exists 

and save the histograms in an ntupple with name branch_name_creteria_bin[i]_bin[i+1]


After all the histograme is done then we will move on to variable creteria for binning.


once the varible bins boundoury are there we want to go into that range and make histogram of Ereco/Etrue and fit it with crujjjfit function.
\

Done with the validation code .

making varyhistogram using that varybinning criteria .

 ./makevaryhistogram rootfile/nTupleMC_Merged.root energy workspace2/output.txt workspace2/hist.root T nTuplelize

fit with cruijjf fucntion.


code:python3 fit.py input_path  histogram_name criteria_name resolution_name
example code :python3 fit.py workspace2/output.root energy 1000 0.5 output_path        [please take same value]

Introduction
This framework facilitates the analysis of particle physics data by converting raw data into histograms and performing various analyses. This README provides instructions on how to use the framework effectively.

Getting Started
Installation
To get started, create a Conda environment using the provided environment.yml file:
conda env create -f environment.yml
This command will set up the necessary environment for running the framework.

Usage
Compile the C++ code:
make
Run the code to create histograms:
make run ARGS="input_file.root workspace"
Replace input_file.root with the path to your root file and workspace with the desired workspace directory.

You can also specify a specific directory within the root file:
make run ARGS="input_file.root workspace specific_directory"


After running the code, histograms will be generated and saved in the specified workspace directory.


Usage Example
./bincrithitogram /path/to/input_file.root /path/to/workspace energy 1000 100000 T nTuplelize
Arguments:

/path/to/input_file.root: Path to the input root file.
/path/to/workspace: Path to the workspace directory.
energy: Branch name for histogram generation.
1000: Initial bin size guess.
100000: Criteria for histogram generation.
T: Optional directory name within the root file.
nTuplelize: Optional flag for histogram naming.

Contributing
Contributions to this framework are welcome! Feel free to fork the repository, make your changes, and submit a pull request.





./makevaryhistogram rootfile/nTupleMC_Merged.root energy workspace2/output.txt workspace2/hist.root T nTuplelize


 python3 Validating.py 




new readme


 ./newcode /home/gurshara/myframework/rootfile/nTupleMC_Merged.root /home/gurshara/myframework/workspace2/output.root energy 20 0.5 T /home/gurshara/myframework/workspace2/output.txt nTuplelize

it will create the binning file 

now i will use the binning file and make histogram and save enteries in each bin 

 
 ./makevaryhistogram rootfile/nTupleMC_Merged.root Ele_Gen_E workspace2/output.txt workspace2/hist.root T nTuplelize [making the same binning]


 ./makeandsavehist rootfile/nTupleMC_Merged.root Ele_Gen_E workspace2/output.txt workspace2/hist.root T testing_my_w  nTuplelize

this will make and savehistogram into the file 


now using that file i will fit the ratio_plot .


python3 fit.py 


later using that dataframe we will plot means plot the means.

ploting.py














source newcode.sh

 ./newcode /home/gurshara/myframework/rootfile/nTupleMC_Merged.root /home/gurshara/myframework/workspace2/output.root energy 10000 10 T /home/gurshara/myframework/workspace2/output.txt nTuplelize


 source buildhist.sh
 ./buildhist


 it will make a response_histograms.root file 

which will fit via test.py  which is build in locally  

python test.py ../response_histograms.root  fittedplots/output.root fittedplots/output.txt


 and it will create a file which stores all the mean and sigma

python plot.py fittedplots/output.txt sigmabyE.png



 ploting