#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>

// Function to find the bin index for a given value
int FindBin(std::vector<float>& Grid, float value) {
    int size = Grid.size();
    for (int iii = 0; iii < size - 1; iii++) {
        if (value >= Grid[iii] && value < Grid[iii + 1])
            return iii;
    }
    return -1;
}

// Function to read binning values from a file
std::vector<float> readBinningFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Error: Unable to open file " << filename << std::endl;
        return std::vector<float>(); // Return an empty vector on error
    }

    std::vector<float> binning;
    float binEdge;
    
    while (file >> binEdge) {
        binning.push_back(binEdge);
    }

    file.close();
    return binning;

}

// Function to define and fill response histograms
void defineResponseHistograms(std::vector<float>& binning) {
    TFile inputFile("rootfile/nTupleMC_Merged.root", "READ");

    std::string fullTreeName = "nTuplelize/T";
    TTree* tree = dynamic_cast<TTree*>(inputFile.Get(fullTreeName.c_str()));
    if (!tree) {
        std::cerr << "Could not find the tree. Exiting." << std::endl;
        return;
    }

    const int num_resp_bins = binning.size() - 1;
    std::vector<TH1F*> response(num_resp_bins);

    for (int i = 0; i < num_resp_bins; i++) {
        char name[50];
        char title[50];
       sprintf(name, "response_%.3f_%.3f", static_cast<double>(binning[i]),  static_cast<double>(binning[i+1]) );
        sprintf(title, "response_%.3f_%.3f", static_cast<double>(binning[i]), static_cast<double>(binning[i+1])  );
        response[i] = new TH1F(name, title, 100, 0.8, 1.2);
    }

    int recoelectron;
    tree->SetBranchAddress("nElectrons", &recoelectron);

    std::vector<float> *branchvalue = 0;
    tree->SetBranchAddress("energy", &branchvalue);

    std::vector<float> *branchvalue2 = 0;
    tree->SetBranchAddress("Ele_Gen_E", &branchvalue2);

    std::vector<float> *eta = 0;
    tree->SetBranchAddress("eta", &eta);

    std::vector<float> *phi = 0;
    tree->SetBranchAddress("phi", &phi);

    for (int i = 0; i < tree->GetEntries(); i++) {          //tree->GetEntries()    
        tree->GetEntry(i);

        if(recoelectron != 2){
            continue;
        }

        if ((eta->at(0) > -2.5 && eta->at(0) < -2) && (phi->at(0) > -1.7 && phi->at(0) < -1.2)) {
            continue;
        }



        float response_val = branchvalue->at(0) / branchvalue2->at(0);

       std::cout<<i<<std::endl;
       
        int bin = FindBin(binning, branchvalue->at(0));

        


        if (bin != -1) {
            response[bin]->Fill(response_val);
        }
    }




    std::cout<<"hi i am on way to saving the histogarms";


    

    // Write histograms to a new ROOT file
    TFile outputFile("response_histograms.root", "RECREATE");
    for (int i = 0; i < num_resp_bins; i++) {
        response[i]->Write();
        delete response[i];  // Clean up dynamically allocated histograms
    }
    outputFile.Close();
}

int main() {
    std::string binningFilename = "workspace2/output.txt";

    std::vector<float> binning = readBinningFile(binningFilename);

     for (float value : binning) {
            std::cout << value << std::endl;
        }



    if (!binning.empty()) {
        defineResponseHistograms(binning);
    }

    return 0;
}
