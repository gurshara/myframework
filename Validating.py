import uproot3
import numpy as np

# Open the ROOT file
file = uproot3.open("workspace2/output.root")

# Access the histogram named 'energy'
hist = file["energy"]

# Get the bin edges and contents
bin_edges = hist.edges
bin_contents = hist.values

# Print the bin edges and contents
print("Bin edges:", bin_edges)
print("Bin contents:", bin_contents)

# If you want to pair each bin content with its corresponding bin center
bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
for center, content in zip(bin_centers, bin_contents):
    print(f"Bin center: {center}, Bin content: {content}")