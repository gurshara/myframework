import sys
import pandas as pd
import matplotlib.pyplot as plt

def plot_data(dataframe_path, x_column, y_column, output_dir):
    # Load the DataFrame from the CSV file
    df = pd.read_csv(dataframe_path)
    
    # Check if the specified columns exist in the DataFrame
    if x_column not in df.columns or y_column not in df.columns:
        print(f"Error: Columns {x_column} or {y_column} not found in the DataFrame.")
        sys.exit(1)
    
    # Plotting the data
    plt.figure(figsize=(10, 6))
    plt.plot(df[x_column], df[y_column], marker='o', linestyle='-', color='b')
    plt.xlabel(x_column)
    plt.ylabel(y_column)
    plt.title(f'{y_column} vs {x_column}')
    plt.grid(True)
    
    # Save the plot
    output_file = f"{output_dir}/{y_column}_vs_{x_column}.png"
    plt.savefig(output_file)
    plt.close()
    
    print(f"Plot saved as {output_file}")

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: python plotting.py <dataframe_path> <x_column> <y_column> <output_dir>")
        sys.exit(1)
    
    dataframe_path = sys.argv[1]
    x_column = sys.argv[2]
    y_column = sys.argv[3]
    output_dir = sys.argv[4]
    
    plot_data(dataframe_path, x_column, y_column, output_dir)
