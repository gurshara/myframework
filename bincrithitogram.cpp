#include <iostream>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include <sstream>

void filterEvents(const std::string& inputFileName, const std::string& outputFileName, const std::string& branchName, int criteria, int binSize,const std::string& tree_name, const std::string& directoryName = "") {
    // Open the input ROOT file
    TFile inputFile(inputFileName.c_str(), "READ");

    std::cout<<"iam working till here";

    // Access the directory if provided
    TDirectory* directory = nullptr;
    if (!directoryName.empty()) {
        directory = inputFile.GetDirectory(directoryName.c_str());
        if (!directory) {
            std::cerr << "Directory '" << directoryName << "' not found. Exiting." << std::endl;
            return;
        }
    }

    // Access the tree within the directory or directly from the file
    TTree* tree = nullptr;
    if (directory) {
        tree = dynamic_cast<TTree*>(directory->Get(tree_name.c_str()));
    } else {
        tree = dynamic_cast<TTree*>(inputFile.Get(tree_name.c_str()));
    }
    if (!tree) {
        std::cerr << "Could not find the tree" << tree_name<<". Exiting." << std::endl;
        return;
    }

      std::cout<<"iam working till here";

    

    // Create the output ROOT file
    TFile outputFile(outputFileName.c_str(), "RECREATE");

    // Create the histogram with specified bin size
    double min = tree->GetMinimum(branchName.c_str());
    double max = tree->GetMaximum(branchName.c_str());
    int nbins = static_cast<int>((max - min) / binSize) + 1;
    TH1D* histo = new TH1D("Ereco", "Ereco Histogram", nbins, min, max);

    std::cout<<"iam working till here";

    // Loop over events and fill the histogram
    std::vector<float>* value= nullptr;
    int recoelectron;

    tree->SetBranchAddress(branchName.c_str(), &value);
    tree->SetBranchAddress("nElectrons", &recoelectron); 

    std::cout<<"iam working till here";
    float value1;

    
    for (Long64_t i = 0; i < tree->GetEntries(); ++i) {
        tree->GetEntry(i);

        // Check if the value of the Recoelectron branch is not equal to 2
        if (recoelectron != 2) {
            continue;  // Skip this event
        }
        value1=(*value)[2];

        // Fill the histogram with the branch value
        histo->Fill(value1);
    }
    std::cout<<"iam working till here";

    int bin =1;
    int bin2=1;
    int sum=0;

    // Create and fill separate histograms for each bin range that meets the criteria
    while(bin2 <= histo->GetNbinsX()) {
       sum=sum+histo->GetBinContent(bin2) ;
        while(sum < criteria) {
            bin2=bin2+1;
            sum=sum+histo->GetBinContent(bin2) ;
            if(bin2 > histo->GetNbinsX()){
                break;
            }
            }
            if(sum!=0 && bin2 > histo->GetNbinsX()){

            double binLow = histo->GetBinLowEdge(bin);
            double binHigh = histo->GetBinUpEdge(bin2);
            std::stringstream ss;
            ss << branchName << "_" << criteria << "_bin" << binLow << "_" << binHigh;
            TH1D* histoPart = new TH1D(ss.str().c_str(), ss.str().c_str(), 1, binLow, binHigh);
            histoPart->SetBinContent(1, histo->GetBinContent(bin));
            histoPart->Write();


            }else if(sum!=0 && bin2 <= histo->GetNbinsX()){
            double binLow = histo->GetBinLowEdge(bin);
            double binHigh = histo->GetBinLowEdge(bin2);
            std::stringstream ss;
            ss << branchName << "_" << criteria << "_bin" << binLow << "_" << binHigh;
            TH1D* histoPart = new TH1D(ss.str().c_str(), ss.str().c_str(), 1, binLow, binHigh);
            histoPart->SetBinContent(1, histo->GetBinContent(bin));
            histoPart->Write();

            }
            double binLow = histo->GetBinLowEdge(bin);
            double binHigh = histo->GetBinLowEdge(bin2);
            std::stringstream ss;
            ss << branchName << "_" << criteria << "_bin" << binLow << "_" << binHigh;
            TH1D* histoPart = new TH1D(ss.str().c_str(), ss.str().c_str(), 1, binLow, binHigh);
            histoPart->SetBinContent(1, histo->GetBinContent(bin));
            histoPart->Write();

        sum=0;
        bin=bin2;
        bin2=bin+1;
    }

    // Write the output file
    outputFile.Close();
    inputFile.Close();
}

int main(int argc, char* argv[]) {
    // Check for correct number of command line arguments
    if (argc < 7 || argc > 8) {
        std::cerr << "Usage: " << argv[0] << " input_file output_file branch_name criteria bin_size [directory_name]" << std::endl;
        return 1;
    }

    // Parse command line arguments
    std::string inputFileName = argv[1];
    std::string outputFileName = argv[2];
    std::string branchName = argv[3];
    int criteria = std::stoi(argv[4]);  // Assuming the criteria is an integer
    int binSize = std::stoi(argv[5]);
    std::string tree_name = argv[6];
    
    std::string directoryName = "";
    if (argc == 8) {
        directoryName = argv[7];
    }

    // Call the function to filter events
    filterEvents(inputFileName, outputFileName, branchName, criteria, binSize, tree_name ,directoryName);

    return 0;
}
