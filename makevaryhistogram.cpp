#include <iostream>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include <vector>
#include <fstream>

void createHistogram(const std::string& inputRootFile, const std::string& branchName, const std::string& binningTxtFile, const std::string& outputRootFile, const std::string& treeName, const std::string& parentDirectory = "") {
    // Open the input ROOT file
    TFile inputFile(inputRootFile.c_str(), "READ");

    // Access the tree
    std::string fullTreeName;
    if (!parentDirectory.empty()) {
        fullTreeName = parentDirectory+ "/" + treeName;
    } else {
        fullTreeName = treeName.c_str();
    }
    TTree* tree = dynamic_cast<TTree*>(inputFile.Get(fullTreeName.c_str())); 
    if (!tree) {
        std::cerr << "Could not find the tree. Exiting." << std::endl;
        return;
    }

    // Read binning values from the text file
    std::ifstream binningFile(binningTxtFile.c_str());
    if (!binningFile.is_open()) {
        std::cerr << "Error: Could not open the binning text file." << std::endl;
        return;
    }
    
    std::vector<float> binning;
    float value;
    while (binningFile >> value) {
        binning.push_back(value);
    }
    binningFile.close();

    // Create histogram
    TH1F* hist = new TH1F(branchName.c_str(), branchName.c_str(), binning.size() - 1, &binning[0]);

    int recoelectron;
    tree->SetBranchAddress("nElectrons", &recoelectron); 

    std::vector<float> *branchvalue=0; 
    tree->SetBranchAddress(branchName.c_str(),&branchvalue);
   
    // Loop over entries in the tree and fill histogram
    for (Long64_t i = 0; i <100000 ; ++i) {                                   //tree->GetEntries()
        tree->GetEntry(i);

        // Check if reelectro is equal to 2
        if (recoelectron == 2) {
            // Fill histogram from the branch of interest
            
            hist->Fill(branchvalue->at(0));
        }
    }

    // Write histogram to output ROOT file
    TFile outputFile(outputRootFile.c_str(), "UPDATE");
    hist->Write();
    outputFile.Close();

    // Cleanup
    delete hist;

}

int main(int argc, char* argv[]) {
    // Check for correct number of command line arguments
    if (argc < 6 || argc > 7) {
        std::cerr << "Usage: " << argv[0] << " input_root_file branch_name binning_txt_file output_root_file tree_name [parent_directory]" << std::endl;
        return 1;
    }

    // Parse command line arguments
    std::string inputRootFile = argv[1];
    std::string branchName = argv[2];
    std::string binningTxtFile = argv[3];
    std::string outputRootFile = argv[4];
    std::string treeName = argv[5];
    std::string parentDirectory = "";
    if (argc == 7) {
        parentDirectory = argv[6];
    }

    // Create histogram
    createHistogram(inputRootFile, branchName, binningTxtFile, outputRootFile, treeName, parentDirectory);

    return 0;
}

