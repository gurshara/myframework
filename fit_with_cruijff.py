import os
from python_functions.Cruijff import Cruijff
import numpy as np

def fit_histograms(input_file_name, output_file_name, output_txt_file):
    # Open the input ROOT file
    # Read the ROOT file without ROOT to avoid dependency
    with np.load(input_file_name) as data:
        histograms = data['histograms']
    
    # Prepare the output text file
    with open(output_txt_file, 'w') as txt_file:
        # Write header
        txt_file.write("bin_low bin_high mean sigma mean_error sigma_error\n")
        
        # Loop over all histograms
        for hist_info in histograms:
            # Extract histogram information
            bin_low, bin_high = hist_info['bin_range']
            hist_data = hist_info['data']
            
            # Fit the histogram with the Cruijff function
            # Initial guesses for the parameters
            initial_guesses = Cruijff.popt_guesses(hist_data['bin_centers'], hist_data['bin_heights'], None)
            
            # Perform the fit
            popt, _ = opt.curve_fit(Cruijff.pdf, hist_data['bin_centers'], hist_data['bin_heights'], p0=initial_guesses)
            
            # Extract fit parameters
            mean, mean_error = Cruijff.mean(popt, np.sqrt(np.diag(pcov)))
            resolution, resolution_error = Cruijff.resolution(popt, np.sqrt(np.diag(pcov)))
                
            # Write the results to the text file
            txt_file.write(f"{bin_low} {bin_high} {mean} {resolution} {mean_error} {resolution_error}\n")

            # Write the histogram to the output ROOT file
            # Assuming you don't need to write to a ROOT file anymore
            
    print(f"Results written to {output_txt_file}")

if __name__ == "__main__":
    import sys
    
    if len(sys.argv) != 4:
        print("Usage: python fit_histograms.py <input_file> <output_file> <output_txt_file>")
        sys.exit(1)
    
    input_file_name = sys.argv[1]
    output_file_name = sys.argv[2]
    output_txt_file = sys.argv[3]
    
    fit_histograms(input_file_name, output_file_name, output_txt_file)
