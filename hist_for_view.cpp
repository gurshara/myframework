#include <iostream>
#include <vector>
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TROOT.h>

void processTree(const std::string& filename) {
    // Open the ROOT file
    TFile *file = TFile::Open(filename.c_str(), "READ");
    if (!file || file->IsZombie()) {
        std::cerr << "Error: Unable to open file " << filename << std::endl;
        return;
    }

    // Navigate to the directory containing the tree
    TDirectory *dir = file->GetDirectory("nTuplelize");
    if (!dir) {
        std::cerr << "Error: Unable to find directory Ntuplelize" << std::endl;
        file->Close();
        return;
    }

    // Access the tree
    TTree *tree = nullptr;
    dir->GetObject("T", tree);
    if (!tree) {
        std::cerr << "Error: Unable to find tree T" << std::endl;
        file->Close();
        return;
    }

    // Set up branches
    std::vector<float> *Eraw = 0;
    std::vector<float> *Ereco = 0;
    std::vector<float> *Etrue = 0;
    std::vector<float> *Eta = 0;
    std::vector<float> *R9 = 0;
    std::vector<float> *Phi = 0;
    float rho;
    int recoelectron;

    tree->SetBranchAddress("Ele_SCRawE", &Eraw);
    tree->SetBranchAddress("energy", &Ereco);
    tree->SetBranchAddress("Ele_Gen_E", &Etrue);
    tree->SetBranchAddress("eta", &Eta);
    tree->SetBranchAddress("Ele_R9", &R9);
    tree->SetBranchAddress("rho", &rho);
    tree->SetBranchAddress("phi", &Phi);
    tree->SetBranchAddress("nElectrons", &recoelectron); 

    // Create histograms
    TH1F *EB_Ereco_true = new TH1F("EB_Ereco_true", "Ereco EB", 100, 0.7,1.2); // Adjust binning as needed
    TH1F *EB_Eraw_true = new TH1F("EB_Eraw_true", "Eraw EB", 100, 0.7, 1.2); // Adjust binning as needed
    TH1F *EE_Ereco_true = new TH1F("EE_Ereco_true", "Ereco EE", 100, 0.7, 1.2); // Adjust binning as needed
    TH1F *EE_Eraw_true = new TH1F("EE_Eraw_true", "Eraw EE", 100, 0.7, 1.2); // Adjust binning as needed

    TH1F *hEta = new TH1F("Eta", "Eta", 100, -3.5, 3.5); // Adjust binning as needed
    TH1F *hR9 = new TH1F("R9", "R9", 100, 0, 1.5); // Adjust binning as needed
    TH1F *hPhi = new TH1F("Phi", "Phi", 100, -4, 4); // Adjust binning as needed
    TH1F *hrho = new TH1F("rho", "rho", 100, 0, 80);

    // Loop over entries and fill histograms
    Long64_t nentries = tree->GetEntries();
    for (Long64_t i = 0; i < nentries; ++i) {
        tree->GetEntry(i);

        if(i%10000==0){
            std::cout<<i<<std::endl;
        }

        if (recoelectron != 2) {
            continue;
        }

        float raw_true = Eraw->at(0) / Etrue->at(0);
        float reco_true = Ereco->at(0) / Etrue->at(0);

        if (fabs(Eta->at(0)) < 1.4442) {   // EB REGION
            EB_Ereco_true->Fill(reco_true);
            EB_Eraw_true->Fill(raw_true);
        }

        if (fabs(Eta->at(0)) > 1.566) {   // EE REGION
            EE_Ereco_true->Fill(reco_true);
            EE_Eraw_true->Fill(raw_true);
        }

        hEta->Fill(Eta->at(0));
        hR9->Fill(R9->at(0));
        hPhi->Fill(Phi->at(0));
        hrho->Fill(rho);
    }

    // Save histograms to a new file
    TFile *outputFile = new TFile("histograms.root", "RECREATE");
    EB_Ereco_true->Write();
    EE_Ereco_true->Write();
    EB_Eraw_true->Write();
    EE_Eraw_true->Write();
    hEta->Write();
    hR9->Write();
    hPhi->Write();
    hrho->Write();
    outputFile->Close();

    // Normalize histograms
    EB_Ereco_true->Scale(1.0 / EB_Ereco_true->Integral());
    EB_Eraw_true->Scale(1.0 / EB_Eraw_true->Integral());
    EE_Ereco_true->Scale(1.0 / EE_Ereco_true->Integral());
    EE_Eraw_true->Scale(1.0 / EE_Eraw_true->Integral()); 

    // Create and draw comparison plots on the same canvas
    TCanvas *c1 = new TCanvas("c1", "EB Comparison", 800, 600);
    EB_Ereco_true->SetLineColor(kRed);
    EB_Eraw_true->SetLineColor(kBlue);
    EB_Ereco_true->Draw("LP");
    EB_Eraw_true->Draw("SAME LP");
    TLegend *legend1 = new TLegend(0.7, 0.8, 0.9, 0.9);
    legend1->AddEntry(EB_Ereco_true, "EB_Ereco_true", "l");
    legend1->AddEntry(EB_Eraw_true, "EB_Eraw_true", "l");
    legend1->Draw();
    c1->SaveAs("EB_Comparison.png");

    TCanvas *c2 = new TCanvas("c2", "EE Comparison", 800, 600);
    EE_Ereco_true->SetLineColor(kRed);
    EE_Eraw_true->SetLineColor(kBlue);
    EE_Ereco_true->Draw();
    EE_Eraw_true->Draw("SAME");
    TLegend *legend2 = new TLegend(0.7, 0.8, 0.9, 0.9);
    legend2->AddEntry(EE_Ereco_true, "EE_Ereco_true", "l");
    legend2->AddEntry(EE_Eraw_true, "EE_Eraw_true", "l");
    legend2->Draw();
    c2->SaveAs("EE_Comparison.png");

    // Clean up
    delete EB_Ereco_true;
    delete EE_Ereco_true;
    delete EB_Eraw_true;
    delete EE_Eraw_true;
    delete hEta;
    delete hR9;
    delete hPhi;
    file->Close();
    delete file;
}


int main() {
    std::string filename = "rootfile/nTupleMC_Merged.root";
    processTree(filename);
    return 0;
}
