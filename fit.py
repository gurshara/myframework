import ROOT
import os
from python_functions.Cruijff import Cruijff

def fit_histograms(input_file_name, output_file_name, output_txt_file):
    # Open the input ROOT file
    input_file = ROOT.TFile(input_file_name, "READ")
    if input_file.IsZombie():
        print(f"Error: Could not open the input file {input_file_name}")
        return
    
    # Prepare the output text file
    with open(output_txt_file, 'w') as txt_file:
        # Write header
        txt_file.write("bin_low bin_high mean sigma mean_error sigma_error\n")
        
        # Get list of keys in the input file
        keys = input_file.GetListOfKeys()
        
        # Loop over all keys in the file
        for key in keys:
            obj = key.ReadObj()
            # Check if the object is a histogram and has the expected name format
            if isinstance(obj, ROOT.TH1) and obj.GetName().startswith("response_"):
                # Fit the histogram with a Gaussian function
                gaus_fit = ROOT.TF1("gaus_fit", "gaus")
                fit_result = obj.Fit(gaus_fit, "S")

                # Extract fit parameters
                mean = gaus_fit.GetParameter(1)
                sigma = gaus_fit.GetParameter(2)
                mean_error = gaus_fit.GetParError(1)
                sigma_error = gaus_fit.GetParError(2)
                
                # Extract bin range from histogram name
                name_parts = obj.GetName().split('_')
                bin_low = float(name_parts[1])
                bin_high = float(name_parts[2])
                
                # Write the results to the text file
                txt_file.write(f"{bin_low} {bin_high} {mean} {sigma} {mean_error} {sigma_error}\n")

                # Write the histogram to the output ROOT file
                output_file.cd()
                obj.Write()

    # Close the input and output ROOT files
    input_file.Close()
    output_file.Close()

if __name__ == "__main__":
    import sys
    
    if len(sys.argv) != 4:
        print("Usage: python fit_histograms.py <input_file> <output_file> <output_txt_file>")
        sys.exit(1)
    
    input_file_name = sys.argv[1]
    output_file_name = sys.argv[2]
    output_txt_file = sys.argv[3]
    
    # Create the output ROOT file
    output_file = ROOT.TFile(output_file_name, "RECREATE")
    if output_file.IsZombie():
        print(f"Error: Could not create the output file {output_file_name}")
        sys.exit(1)
    
    fit_histograms(input_file_name, output_file_name, output_txt_file)
    print(f"Results written to {output_txt_file}")
