#include <iostream>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include <sstream>
#include <vector>
#include <algorithm>
#include <fstream> 

void filterEvents(const std::string& inputFileName, const std::string& outputFileName, const std::string& branchName, int criteria, int resolution, const std::string& tree_name, const std::string& binningFilePath,const std::string& binningFilePath2, const std::string& directoryName = "") {

    // Open the input ROOT file
    TFile inputFile(inputFileName.c_str(), "READ");

    std::cout<<"iam working till here"<<std::endl;

    // Access the directory if provided
    TDirectory* directory = nullptr;
    if (!directoryName.empty()) {
        directory = inputFile.GetDirectory(directoryName.c_str());
        if (!directory) {
            std::cerr << "Directory '" << directoryName << "' not found. Exiting." << std::endl;
            return;
        }
    }

    // Access the tree within the directory or directly from the file
    TTree* tree = nullptr;
    if (directory) {
        tree = dynamic_cast<TTree*>(directory->Get(tree_name.c_str()));
    } else {
        tree = dynamic_cast<TTree*>(inputFile.Get(tree_name.c_str()));
    }
    if (!tree) {
        std::cerr << "Could not find the tree " << tree_name << ". Exiting." << std::endl;
        return;
    }

    std::cout<<"iam working till here"<<std::endl;

    // Create the output ROOT file
    TFile outputFile(outputFileName.c_str(), "RECREATE");
    std::cout<<"iam working till here"<<std::endl;

    // Loop over events and fill the histogram
    std::vector<float> *value=0; 
    int recoelectron;
    std::vector<float> *pt=0; 
    std::vector<float> *eta;
    std::vector<float> *R9 = 0;

    tree->SetBranchAddress(branchName.c_str(), &value);
    tree->SetBranchAddress("nElectrons", &recoelectron); 
    tree->SetBranchAddress("pt", &pt); 
    tree->SetBranchAddress("Ele_R9", &R9);

    tree->SetBranchAddress("eta ", &eta);

    std::vector<float> histo;
    std::vector<float> histo2;

    
    for (Long64_t i = 0; i < tree->GetEntries(); ++i) {               //need to be edited here i should replace 100000 with tree->GetEntries()
        tree->GetEntry(i);

        // Check if the value of the RecoElectron branch is not equal to 2
        if (recoelectron != 2) {
            continue;  // Skip this event
        }
        
        if(pt->at(0)<10){
            continue;
            
        }

        if( ( fabs(eta->at(0)) > 1.566 )){  

        std::cout<<value->at(0)<<std::endl;
        histo.push_back(value->at(0));  }

        if( ( fabs(eta->at(0)) < 1.4442 )){
        std::cout<<value->at(0)<<std::endl;
        histo2.push_back(value->at(0));

        }


    }






    std::cout<<"before sorting"<<std::endl;

    std::sort(histo.begin(), histo.end());
    std::sort(histo2.begin(), histo2.end());
    std::cout<<"After sorting"<<std::endl;

    
for(int i=0;i<histo.size();i++){
    std::cout<<histo[i]<<std::endl;
}
    std::cout<<"iam working till here"<<std::endl;

    int j=0;
    int k=criteria;
    std::vector<float> binning;


      if(histo.size()<k){
        binning.push_back(histo[0]);
        binning.push_back(histo[histo.size()]);
        //criteria not met
    }


    while(k<histo.size()){
    if (std::fabs((histo[k] - histo[j])) >resolution) {
        binning.push_back(histo[j]);
        binning.push_back(histo[k]);
    }else{

        while( std::fabs((histo[k] - histo[j]))<resolution){
            k++;
            if(k>histo.size()){
                break;
            }
        }

        if(k>histo.size()){

            break;
    }else{

        binning.push_back(histo[j]);
        binning.push_back(histo[k]);

    }}


    std::cout<<histo[j]<<" ";

    j=k;
    k=j+criteria;
    

    }


    if(k>histo.size()){
        binning.pop_back();
        binning.push_back(histo[histo.size()-1]);
    }

    std::cout<<"iam working till here"<<std::endl;



    auto last = std::unique(binning.begin(), binning.end());
    
    // Erase the duplicate elements
    binning.erase(last, binning.end());
    
    // Print the result
    for(double val : binning) {
        std::cout << val << " ";
    }


for(int i=0;i<histo2.size();i++){
    std::cout<<histo2[i]<<std::endl;
}
    std::cout<<"iam working till here"<<std::endl;

    int j2=0;
    int k2=criteria;
    std::vector<float> binning2;


      if(histo2.size()<k){
        binning2.push_back(histo2[0]);
        binning2.push_back(histo2[histo2.size()]);
        //criteria not met
    }


    while(k2<histo2.size()){
    if (std::fabs((histo2[k] - histo2[j])) >resolution) {
        binning2.push_back(histo2[j]);
        binning2.push_back(histo2[k]);
    }else{

        while( std::fabs((histo2[k] - histo2[j]))<resolution){
            k2++;
            if(k2>histo2.size()){
                break;
            }
        }

        if(k2>histo2.size()){

            break;
    }else{

        binning2.push_back(histo2[j]);
        binning2.push_back(histo2[k]);

    }}


    std::cout<<histo2[j]<<" ";

    j2=k2;
    k2=j2+criteria;
    

    }


    if(k2>histo2.size()){
        binning2.pop_back();
        binning2.push_back(histo2[histo2.size()-1]);
    }

    std::cout<<"iam working till here"<<std::endl;



    auto last2 = std::unique(binning2.begin(), binning2.end());
    
    // Erase the duplicate elements
    binning2.erase(last2, binning2.end());
    
    // Print the result
    for(double val : binning2) {
        std::cout << val << " ";
    }



    std::ofstream binningFile(binningFilePath);
    if (!binningFile.is_open()) {
        std::cerr << "Error: Could not open the file " << binningFilePath << std::endl;
        return;
    }

    // Write the binning values to the text file
    for (double val : binning) {
        binningFile << val << " ";
    }

    // Close the text file
    binningFile.close();





    std::ofstream binningFile2(binningFilePath2);
    if (!binningFile2.is_open()) {
        std::cerr << "Error: Could not open the file " << binningFilePath2 << std::endl;
        return;
    }

    // Write the binning values to the text file
    for (double val : binning2) {
        binningFile2 << val << " ";
    }

    // Close the text file
    binningFile2.close();


    TH1F* hist = new TH1F(branchName.c_str(), branchName.c_str(), binning.size() -1, &binning[0]);

    std::cout<<"iam working till here"<<std::endl;

    // Fill the histogram with some example values
    for (float value : histo) {
        hist->Fill(value);
        
    }

    std::cout<<"iam working till here"<<std::endl;

    hist->Write();
    
    // Write the output file
    outputFile.Close();
    inputFile.Close();
}

int main(int argc, char* argv[]) {
    // Check for correct number of command line arguments
    if (argc < 9 || argc > 10) {
        std::cerr << "Usage: " << argv[0] << " input_file output_file branch_name criteria bin_size tree_name binning_file_path [directory_name]" << std::endl;
        return 1;
    }

    // Parse command line arguments
    std::string inputFileName = argv[1];
    std::string outputFileName = argv[2];
    std::string branchName = argv[3];
    int criteria = std::stoi(argv[4]);  // Assuming the criteria is an integer
    int resolution = std::stoi(argv[5]);  // Assuming the resolution is an integer
    std::string tree_name = argv[6];
    std::string binningFilePath = argv[7];
    std::string binningFilePath2 = argv[8];
    
    std::string directoryName = "";
    if (argc == 10) {
        directoryName = argv[9];
    }

    // Call the function to filter events
    filterEvents(inputFileName, outputFileName, branchName, criteria, resolution, tree_name, binningFilePath, binningFilePath2,directoryName);

    return 0;
}