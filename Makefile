# Compiler settings
CXX = g++
CXXFLAGS = -std=c++11 -Wall

# ROOT dependencies
ROOTCFLAGS := $(shell root-config --cflags)
ROOTLIBS := $(shell root-config --libs)

# Source files and executable
SRCS = create_histograms.cpp
OBJS = $(SRCS:.cpp=.o)
EXE = create_histograms

# Targets
all: $(EXE)

$(EXE): $(OBJS)
	$(CXX) $(CXXFLAGS) $(ROOTLIBS) -o $@ $^

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(ROOTCFLAGS) -c -o $@ $<

run: $(EXE)
	./$(EXE) $(ARGS)

clean:
	rm -f $(OBJS) $(EXE)
