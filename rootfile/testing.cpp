#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

int main() {
    // Open the ROOT file containing the TTree
    TFile inputFile("nTupleMC_Merged.root", "READ");
    if (!inputFile.IsOpen()) {
        std::cerr << "Error: Could not open input file." << std::endl;
        return 1;
    }

    // Access the directory containing the TTree
    TDirectory* directory = inputFile.GetDirectory("nTuplelize");
    if (!directory) {
        std::cerr << "Error: Could not retrieve directory 'nTuplelize' from input file." << std::endl;
        inputFile.Close();
        return 1;
    }

    // Access the TTree within the directory
    TTree* tree = dynamic_cast<TTree*>(directory->Get("T"));
    if (!tree) {
        std::cerr << "Error: Could not retrieve TTree from directory 'nTuplelize'." << std::endl;
        inputFile.Close();
        return 1;
    }

    // Access the energy branch of the TTree
    std::vector<float> energy;
    TBranch* energyBranch = tree->GetBranch("energy");
    if (!energyBranch) {
        std::cerr << "Error: Could not retrieve energy branch from TTree." << std::endl;
        inputFile.Close();
        return 1;
    }

    // Access the nElectrons branch of the TTree
    int recoelectron;
    TBranch* recoelectronBranch = tree->GetBranch("nElectrons");
    if (!recoelectronBranch) {
        std::cerr << "Error: Could not retrieve nElectrons branch from TTree." << std::endl;
        inputFile.Close();
        return 1;
    }

    // Set the address of the energy branch to the energy vector
    energyBranch->SetAddress(&energy);
    recoelectronBranch->SetAddress(&recoelectron);

    // Get the number of entries in the TTree
    Long64_t numEntries = tree->GetEntries();

    // Loop over entries in the TTree
    for (Long64_t i = 0; i < numEntries; ++i) {
        tree->GetEntry(i);

        // Check if the value of the recoelectron branch is 2
        if (recoelectron == 2) {
            // Print the first element of the energy vector
            if (!energy.empty()) {
                std::cout << "First element of energy vector: " << energy[] << std::endl;
                break;
            }
        }
    }

    // Close the input file
    inputFile.Close();

    return 0;
}
